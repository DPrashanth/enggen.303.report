FILENAMEBASE := report
BIN := bin
PDF := output_pdf
push:
	git push origin master &
pdf:
	mkdir -p $(BIN)
	pdflatex -output-directory $(BIN) $(FILENAMEBASE).tex 
	#bibtex $(BIN)/$(FILENAMEBASE).aux
	#pdflatex -output-directory $(BIN) $(FILENAMEBASE).tex
	#pdflatex -output-directory $(BIN) $(FILENAMEBASE).tex
	
	mkdir -p $(PDF)
	mv $(BIN)/$(FILENAMEBASE).pdf $(PDF)/$(FILENAMEBASE).pdf
	

pdf-noprint:
	mkdir -p $(BIN)
	cp $(FILENAMEBASE).tex $(FILENAMEBASE)_noprint.tex
	
	# Fix page geometries
	perl -pi -e 's/\\usepackage\[inner=4cm,outer=2cm,vmargin=3.54cm\]{geometry}/\\usepackage\[hmargin=2cm,vmargin=3.54cm\]{geometry}/g' $(FILENAMEBASE)_noprint.tex
	perl -pi -e 's/\\newgeometry{inner=4cm,outer=2cm,vmargin=1.5cm}/\\newgeometry{hmargin=2cm,vmargin=1.5cm}/g' $(FILENAMEBASE)_noprint.tex
	
	# Removed width ajustments
	perl -pi -e 's/\\begin{adjustwidth}{.*}{.*}//g' $(FILENAMEBASE)_noprint.tex
	perl -pi -e 's/\\end{adjustwidth}//g' $(FILENAMEBASE)_noprint.tex
	
	pdflatex -output-directory $(BIN) $(FILENAMEBASE)_noprint.tex 
	
	rm $(FILENAMEBASE)_noprint.tex
	
	mkdir -p $(PDF)
	mv $(BIN)/$(FILENAMEBASE)_noprint.pdf $(PDF)/$(FILENAMEBASE)_noprint.pdf
	

publish: push pdf
	scp $(PDF)/$(FILENAMEBASE).pdf root@128.199.241.174:/var/www/html &

clean:
	rm $(BIN)/*
	rm $(PDF)/*
	
fix-quotes:
	# possibly create a new target that gets rid of stupid characters like:
	# the apostrope/close single quote ’ (replace by ')
	# open single quote ‘ (`)
	# open double quotes “ (``)
	# close double quotes ” ('')
	perl -pi -e "s/’/'/g" $(FILENAMEBASE).tex
	perl -pi -e 's/‘/`/g' $(FILENAMEBASE).tex
	perl -pi -e 's/“/``/g' $(FILENAMEBASE).tex
	perl -pi -e "s/”/''/g" $(FILENAMEBASE).tex
	perl -pi -e 's/ﬁ/fi/g' $(FILENAMEBASE).tex
	perl -pi -e 's/ﬂ/fl/g' $(FILENAMEBASE).tex
	perl -pi -e 's/–/-/g' $(FILENAMEBASE).tex





	
